﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Helper;
using WorkFromHome.Hubs;
using WorkFromHome.Models;
using WorkFromHome.ViewModel;

namespace WorkFromHome.Controllers
{
    [CustomAuthorize("Administration")]
    public class AdminController : Controller
    {
        WFHContext db = new WFHContext();
        // GET: Admin
        public ActionResult Index()
        {
            DateTime date = DateTime.Now;
                var mylist = db.Attendances.Where(x => x.AttendanceDate.Value.Day == date.Day && x.AttendanceDate.Value.Month == date.Month && x.AttendanceDate.Value.Year == date.Year).Include(x => x.User).ToList();
            return View(mylist);
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult ReportsList(int month)
        {
            
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.DailyReports.Where(x =>  x.Date.Value.Month == month && x.Date.Value.Year == date.Year).Include(x => x.User).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.DailyReports.Where(x => x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.User).ToList();
                return PartialView(mylist);
            }

        }

        public ActionResult Tasks()
        {
            return View();
        }

        public ActionResult TasksList(int month)
        {
           
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.TaskDetails.Where(x =>  x.Date.Value.Month == month && x.Date.Value.Year == date.Year).Include(x => x.TaskTo).Include(x => x.TaskBy).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.TaskDetails.Where(x =>  x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.TaskTo).Include(x => x.TaskBy).ToList();
                return PartialView(mylist);
            }

        }

        public ActionResult Attendance()
        {
            return View();
        }

        public ActionResult AttendanceList(int month)
        {
           
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.Attendances.Where(x => x.AttendanceDate.Value.Month == month && x.AttendanceDate.Value.Year == date.Year).Include(x => x.User).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.Attendances.Where(x =>  x.AttendanceDate.Value.Month == date.Month && x.AttendanceDate.Value.Year == date.Year).Include(x => x.User).ToList();
                return PartialView(mylist);
            }

        }

        SqlDependency dependency = null;
        //To set the dependency
        public JsonResult getTaskLists()
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["NotificationContext"].ConnectionString))
                {
                    con.Open();
                    //query for dependency when new data insert it call
                    using (SqlCommand command = new SqlCommand(@"SELECT [TaskID],[Task],[TaskResponse]
                                                                  FROM [dbo].[TaskDetails]
                                                                 WHERE [TaskResponse]=''", con))
                    {
                        // Make sure the command object does not already have
                        // a notification object associated with it.
                        command.Notification = null;
                        if (dependency == null)
                        {
                            dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                        }

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }
        //dependency change method
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (dependency != null)
            {
                dependency.OnChange -= dependency_OnChange;
                //     dependency = null;
            }

            if (e.Type == SqlNotificationType.Change)
            {
                TaskHub.Show();
            }
        }
        //Method to get Notification Data value
        public JsonResult getTaskDetails()
        {
            NotificationVM notificationVM = new NotificationVM();
            //Get Last Insert record
            var data = db.TaskDetails.Include(x => x.TaskBy).OrderByDescending(x => x.TaskID).Take(1).SingleOrDefault();
            //Token for Last TaskAssigned user
            var token = db.FCMTokens.Where(x => x.UserID == data.TaskToID).SingleOrDefault();
            notificationVM.Id = data.TaskID;
            notificationVM.TaskBy = data.TaskBy.FullName;
            notificationVM.Task = data.Task;
            notificationVM.Token = token.TokenValue;
            return Json(notificationVM, JsonRequestBehavior.AllowGet);
        }

    }
}