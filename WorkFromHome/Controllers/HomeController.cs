﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Helper;
using WorkFromHome.Models;
using WorkFromHome.ViewModel;

namespace WorkFromHome.Controllers
{
    [CustomAuthorize("Staff")]
    public class HomeController : Controller
    {
        WFHContext db = new WFHContext();

        public ActionResult Index()
        {
            DateTime date = DateTime.Now;
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            var attData = db.Attendances.Where(x => x.UserID == loginUser.UserID && x.AttendanceDate.Value.Day == date.Day && x.AttendanceDate.Value.Month == date.Month && x.AttendanceDate.Value.Year == date.Year).SingleOrDefault();
            if (attData != null)
            {
                ViewBag.CheckOut = attData.CheckOut.Hours + " : " + attData.CheckOut.Minutes + " : " + attData.CheckOut.Seconds;
                ViewBag.CheckIn = attData.CheckIn.Hours + " : " + attData.CheckIn.Minutes + " : " + attData.CheckIn.Seconds;
                ViewBag.AttLabel = "CheckOut";
            }
            else
            {
                ViewBag.AttLabel = "CheckIn";
            }
            return View();
        }

        public ActionResult Attendance()
        {
            return View();
        }

        public ActionResult AttendanceList(int month)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.Attendances.Where(x => x.UserID == loginUser.UserID && x.AttendanceDate.Value.Month == month && x.AttendanceDate.Value.Year == date.Year).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.Attendances.Where(x => x.UserID == loginUser.UserID && x.AttendanceDate.Value.Month == date.Month && x.AttendanceDate.Value.Year == date.Year).ToList();
                return PartialView(mylist);
            }

        }
        [HttpGet]
        public ActionResult UserTask()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            DateTime date = DateTime.Now;

            var mylist = db.TaskDetails.Where(x => x.TaskToID == loginUser.UserID && x.Date.Value.Day == date.Day && x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.TaskBy).ToList();
            return PartialView(mylist);

        }
        [HttpPost]
        public ActionResult UserTask(int taskId, string TaskResponse)
        {

            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            var task = db.TaskDetails.Find(taskId);
            if (task != null)
            {
                task.TaskResponse = TaskResponse;
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
            }
            DateTime date = DateTime.Now;
            var mylist = db.TaskDetails.Where(x => x.TaskToID == loginUser.UserID && x.Date.Value.Day == date.Day && x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.TaskBy).ToList();
            return PartialView(mylist);

        }

        [HttpPost]
        public JsonResult SubmitReport(VMDailyReport vmReport)
        {
            DateTime date = DateTime.Now;
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var Data = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            DailyReport report = db.DailyReports.Where(x => x.UserID == Data.UserID && x.Date.Value.Day == date.Day && x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.User).SingleOrDefault();
            if (report != null)
            {
                if (vmReport.Update == true)
                {
                    report.Subject = vmReport.Subject;
                    report.Report = vmReport.Report;
                    db.Entry(report).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(report, JsonRequestBehavior.AllowGet);
                }

            }
            else if (vmReport.Update == true)
            {
                DailyReport dailyReport = new DailyReport();
                dailyReport.Date = DateTime.Now;
                dailyReport.Report = vmReport.Report;
                dailyReport.Subject = vmReport.Subject;
                dailyReport.UserID = Data.UserID;
                db.DailyReports.Add(dailyReport);
                db.SaveChanges();
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(3, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult ReportsList(int month)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.DailyReports.Where(x => x.UserID == loginUser.UserID && x.Date.Value.Month == month && x.Date.Value.Year == date.Year).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.DailyReports.Where(x => x.UserID == loginUser.UserID && x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).ToList();
                return PartialView(mylist);
            }

        }


        public ActionResult Tasks()
        {
            return View();
        }

        public ActionResult TasksList(int month)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            DateTime date = DateTime.Now;
            int m = date.Month;
            if (month != -1)
            {
                var mylist = db.TaskDetails.Where(x => x.TaskToID == loginUser.UserID && x.Date.Value.Month == month && x.Date.Value.Year == date.Year).Include(x=>x.TaskBy).ToList();
                return PartialView(mylist);
            }
            else
            {
                var mylist = db.TaskDetails.Where(x => x.TaskToID == loginUser.UserID && x.Date.Value.Month == date.Month && x.Date.Value.Year == date.Year).Include(x => x.TaskBy).ToList();
                return PartialView(mylist);
            }

        }


        [HttpPost]
        public JsonResult AddToken(string token)
        {
            //get Current Logged In user
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            //Check already user add in fcmtoken table then replace with new token
            if (db.FCMTokens.Any(x => x.UserID == loginUser.UserID))
            {
                var fcmToken = db.FCMTokens.Where(x => x.UserID == loginUser.UserID).SingleOrDefault();
                fcmToken.TokenValue = token;
                db.Entry(fcmToken).State = EntityState.Modified;
                db.SaveChanges();
            }
            //If user already not added then add user token 
            else
            {
                FCMToken fCMToken = new FCMToken();
                fCMToken.UserID = loginUser.UserID;
                fCMToken.TokenValue = token;
                db.FCMTokens.Add(fCMToken);
                db.SaveChanges();
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}