﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Helper;
using WorkFromHome.Models;

namespace WorkFromHome.Controllers
{
    [CustomAuthorize("Administration")]
    public class DepartmentsController : Controller
    {
        private WFHContext db = new WFHContext();
        // GET: Departments
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DepartmentLists()
        {
            return PartialView(db.Departments.ToList());
        }


        //Add and Update Method
        #region PostMethods

        [HttpPost]
        public JsonResult AddDepartment(Department department)
        {
            var status = false;
            db.Departments.Add(department);
            db.SaveChanges();
            status = true;
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditDepartment(int? id)
        {
            bool status = false;
            if (id == null)
            {
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            return Json(department, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDepartment(Department department)
        {
            var status = false;
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                status = true;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        #endregion


        public JsonResult DeleteConfirmed(int id)
        {
            var status = false;
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();
            status = true;
            return Json(status, JsonRequestBehavior.AllowGet);
        }
    }
}