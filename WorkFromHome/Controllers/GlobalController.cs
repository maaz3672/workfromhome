﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Models;


namespace WorkFromHome.Controllers
{
    public class GlobalController : Controller
    {
        private WFHContext db = new WFHContext();
        // GET: Dropdown
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public JsonResult Roles()
        {
            var roles = db.Roles.ToList();
            return Json(roles, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult Departments()
        {
            var departs = db.Departments.ToList();
            return Json(departs, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogedInUsers()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            return PartialView(loginUser);
        }

        [HttpGet]
        public JsonResult loginUser()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            return Json(loginUser, JsonRequestBehavior.AllowGet);
        }

        public JsonResult userCheckIn()
        {
            try
            {
                DateTime date = DateTime.Now;
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                          .Select(c => c.Value).SingleOrDefault();
                var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
                var attData = db.Attendances.Where(x => x.UserID == loginUser.UserID && x.AttendanceDate.Value.Day == date.Day && x.AttendanceDate.Value.Month == date.Month && x.AttendanceDate.Value.Year == date.Year).SingleOrDefault();
                if (attData != null)
                {
                    attData.CheckOut = DateTime.Now.TimeOfDay;
                    db.Entry(attData).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(attData, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Attendance att = new Attendance();
                    att.AttendanceDate = DateTime.Now;
                    att.CheckIn = DateTime.Now.TimeOfDay;
                    att.UserID = loginUser.UserID;
                    db.Attendances.Add(att);
                    db.SaveChanges();
                    return Json(att, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }
    }
}