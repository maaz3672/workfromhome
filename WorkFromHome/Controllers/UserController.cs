﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Helper;
using WorkFromHome.Models;


namespace WorkFromHome.Controllers
{

    [CustomAuthorize("Administration")]
    public class UserController : Controller
    {
        private WFHContext db = new WFHContext();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UsersLists()
        {
            return PartialView(db.Users.Include(x=>x.Department).Include(x => x.Role).ToList());
        }

        #region PostMethods

        [HttpPost]
        public JsonResult AddUser(User user)
        {
            var status = false;
            db.Users.Add(user);
            db.SaveChanges();
            status = true;
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditUser(int? id)
        {
            bool status = false;
            if (id == null)
            {
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateUser(User user)
        {
            var status = false;
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                status = true;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult DeleteConfirmed(int id)
        {
            var status = false;
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            status = true;
            return Json(status, JsonRequestBehavior.AllowGet);
        }

       

        

    }
}