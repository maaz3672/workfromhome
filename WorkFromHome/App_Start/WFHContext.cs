namespace WorkFromHome.App_Start
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using WorkFromHome.Models;

    public class WFHContext : DbContext
    {
       
        public WFHContext()
            : base("name=WFHContext")
        {
        }
        
         public DbSet<Role> Roles { get; set; }
         public DbSet<Department> Departments { get; set; }
         public DbSet<User> Users { get; set; }
         public DbSet<TaskDetail> TaskDetails { get; set; }
         public DbSet<Attendance> Attendances { get; set; }
         public DbSet<DailyReport> DailyReports { get; set; }
         public DbSet<FCMToken> FCMTokens { get; set; }
    }

  
}