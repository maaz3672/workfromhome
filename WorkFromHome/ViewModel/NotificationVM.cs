﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkFromHome.ViewModel
{
    public class NotificationVM
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string TaskBy { get; set; }
        public string Token { get; set; }
    }
}