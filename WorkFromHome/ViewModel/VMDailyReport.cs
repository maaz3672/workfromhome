﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkFromHome.ViewModel
{
    public class VMDailyReport
    {
        public string Subject { get; set; }
        public string Report { get; set; }
        public bool Update { get; set; }
    }
}