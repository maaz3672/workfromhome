﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkFromHome.Models
{
    [Table("FCMToken")]
    public class FCMToken
    {
        [Key]
        public int Id { get; set; }
        public string TokenValue { get; set; }
      
        [ForeignKey("UserID")]
        public User User { get; set; }
        public int UserID { get; set; }
    }
}