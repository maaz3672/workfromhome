﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkFromHome.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        [ForeignKey("RoleID")]
        public Role Role { get; set; }
        public int RoleID { get; set; }

        [ForeignKey("DepartID")]
        public Department Department { get; set; }
        public int DepartID { get; set; }

    }
}