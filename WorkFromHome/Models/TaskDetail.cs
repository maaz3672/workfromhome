﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkFromHome.Models
{
    public class TaskDetail
    {
        [Key]
        public int TaskID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm}")]
        public Nullable<DateTime> Date { get; set; }
        public string Task { get; set; }
        public string TaskResponse { get; set; }
       
        [ForeignKey("TaskByID")]
        public User TaskBy { get; set; }
        public int TaskByID { get; set; }

        [ForeignKey("TaskToID")]
        public User TaskTo { get; set; }
        public int TaskToID { get; set; }
    }
}