﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkFromHome.Models
{
    public class DailyReport
    {
        [Key]
        public int ReportID { get; set; }
        public Nullable<DateTime> Date { get; set; }

        public string Subject { get; set; }
        public string Report { get; set; }

        [ForeignKey("UserID")]
        public User User { get; set; }
        public int UserID { get; set; }

    }
}